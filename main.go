package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"html"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func upper(data string) string {

	return strings.ToUpper(data)
}

var (
	servername      = "My Personal Minecraft"
	gameDir         = "bedrock-1.19"
	homedir         string
	playercount     = 0
	timeout         = 5
	running         = false
	stopnow         = false
	godmode         = false
	players         []Player
	daytime         int
	weather         string
	levelName       string
	levelDifficulty string
	levelMode       string
)

type Player struct {
	ID       string `json:"ID"`
	Name     string `json:"name"`
	Online   bool   `json:"online"`
	Played   int64  `json:"played"`
	LastSeen int64  `json:"lastseen"`
}

func main() {
	var err error
	homedir, err = os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	getPlayerInfo()
	mainpage := fmt.Sprintf("<!DOCTYPE html>\n<html>\n<head>\n<title>%s</title>\n<style>\nbody {font-size:50px;}\n</style>\n<meta http-equiv=\"refresh\" content=\"0;URL='/hi'\">\n</head><body>Hi.&nbsp;", servername)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))

	})

	http.HandleFunc("/start", func(w http.ResponseWriter, r *http.Request) {
		page = mainpage
		if !running {
			page = fmt.Sprintf("%sStarting the server now<br>", page)
			go func() {
				runMinecraft()
			}()
		} else {
			if playercount == 0 {
				page = fmt.Sprintf("%sThere are %d players currently online, but the service is running and will shutdown in %d minutes<br>", page, playercount, timeout)
			} else {
				if playercount == 1 {
					page = fmt.Sprintf("%sThere is %d player currently online<br>", page, playercount)
				} else {
					page = fmt.Sprintf("%sThere are %d players currently online<br>", page, playercount)
				}
			}
		}
		page = fmt.Sprintf("%s<br><a href=\"/hi\">goback</a><br></body></html>\n", page)
		fmt.Fprintf(w, page)
	})
	http.HandleFunc("/hi", func(w http.ResponseWriter, r *http.Request) {
		page = mainpage
		if !running {
			page = fmt.Sprintf("%s<a href=\"/start\">Start</a> the server now<br>\n", page)
		} else {
			tod := "daytime"
			if daytime > 13000 {
				tod = "nighttime"
			}
			page = fmt.Sprintf("%s<br>Welcome to: %s (%s - %s)<br>It is currently %s (%d), and the weather is %s<br>\n", page, levelName, levelDifficulty, levelMode, tod, daytime, weather)
			if stopnow {
				page = fmt.Sprintf("%s<br>Stopping the server</br>", page)
			}

		}
		t := time.Now().Unix()
		online := false
		for _, p := range players {
			if p.Online {
				online = true
				page = fmt.Sprintf("%s%s online for %d minutes<br>", page, p.Name, int64((t-p.LastSeen)/60))
			}
		}
		if !online && running {
			page = fmt.Sprintf("%sNobody is online. The server will stop in %d minutes\n", page, int(timeout/4))
		}
		page = fmt.Sprintf("%s</body></html>\n", page)
		fmt.Fprintf(w, page)
	})

	http.HandleFunc("/stop", func(w http.ResponseWriter, r *http.Request) {
		page = mainpage
		stopnow = true
		page = fmt.Sprintf("%s</body></html>\n", page)
		fmt.Fprintf(w, page)
	})

	http.HandleFunc("/godmode", func(w http.ResponseWriter, r *http.Request) {
		page = mainpage
		godmode = true
		fmt.Println("Attempting GodMode")
		page = fmt.Sprintf("%s</body></html>\n", page)
		fmt.Fprintf(w, page)
	})
	log.Fatal(http.ListenAndServe("127.0.0.1:8443", nil))
}

func runMinecraft() {
	running = true
	stopnow = false
	godmode = false
	timeout = 20
	os.Chdir(fmt.Sprintf("%s/%s", homedir, gameDir))
	newDir, err := os.Getwd()
	if err != nil {
	}
	fmt.Printf("Current Working Directory: %s\n", newDir)

	cmd := exec.Command("bedrock_server")
	pipe, _ := cmd.StdoutPipe()

	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}

	if err := cmd.Start(); err != nil {
		// handle error
		log.Fatal(err)
	}

	go func() {
		reader := bufio.NewReader(pipe)
		line, err := reader.ReadString('\n')
		for err == nil {
			fmt.Print(line)
			line, err = reader.ReadString('\n')

			// get level name, mode, and difficulty
			levelMatch, _ := regexp.MatchString("Level Name:", line)
			if levelMatch {
				lline := strings.Split(line, " ")
				levelName = lline[len(lline)-1]
			}
			modeMatch, _ := regexp.MatchString("Game mode:", line)
			if modeMatch {
				lline := strings.Split(line, " ")
				levelMode = lline[len(lline)-1]
			}
			difficultyMatch, _ := regexp.MatchString("Difficulty:", line)
			if difficultyMatch {
				lline := strings.Split(line, " ")
				levelDifficulty = lline[len(lline)-1]
			}

			pmatch, _ := regexp.MatchString("^There are *.* players online:", line)
			if pmatch {
				pline := strings.Split(line, " ")
				count := strings.Split(strings.TrimLeft(strings.TrimRight(pline[2], ")"), "("), "/")
				playercount, _ = strconv.Atoi(count[0])
				log.Println("Players online:", playercount)
			}
			connect, _ := regexp.MatchString("Player connected", line)
			if connect {
				t := time.Now().Unix()
				playercount++
				log.Println("Players online:", playercount)
				pinfo := strings.Split(strings.TrimRight(line, "\n"), " ")
				found := false
				for i, p := range players {
					if p.ID == pinfo[len(pinfo)-1] {
						found = true
						players[i].LastSeen = t
						players[i].Online = true
					}
				}
				if !found {
					var p = Player{
						Name:     strings.TrimRight(pinfo[len(pinfo)-3], ","),
						ID:       pinfo[len(pinfo)-1],
						Online:   true,
						LastSeen: t,
					}
					players = append(players, p)
				}
				writeFile()
			}
			disconnect, _ := regexp.MatchString("Player disconnected", line)
			if disconnect {
				t := time.Now().Unix()
				playercount--
				log.Println("Players online:", playercount)
				pinfo := strings.Split(strings.TrimRight(line, "\n"), " ")

				for i, p := range players {
					if p.ID == pinfo[len(pinfo)-1] {
						diff := t - p.LastSeen
						players[i].Played += diff
						players[i].LastSeen = t
						players[i].Online = false
					}
				}
				writeFile()
			}
			weatherquery, _ := regexp.MatchString("Weather state", line)
			if weatherquery {
				wline := strings.Split(strings.TrimRight(line, "\n"), " ")
				weather = wline[len(wline)-1]
			}
			timequery, _ := regexp.MatchString("Daytime is", line)
			if timequery {
				tline := strings.Split(strings.TrimRight(line, "\n"), " ")
				daytime, _ = strconv.Atoi(tline[len(tline)-1])
			}
		}
	}()

	for {
		time.Sleep(5 * time.Second)
		if timeout == 0 || stopnow {
			io.WriteString(stdin, "stop\n")
			break
		}

		if godmode {
			godmode = false
			fmt.Println("God MODE!")
			for _, p := range players {
				if p.Online {
					io.WriteString(stdin, fmt.Sprintf("clear %s\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("teleport %s -60 85 -34\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("give %s cooked_beef 64\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("give %s fireworks 512\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("give %s clock 1\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("give %s compass 1\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("give %s map 1\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("give %s paper 64\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("give %s cartography_table 1\n", p.Name))
					io.WriteString(stdin, fmt.Sprintf("give %s bed 1 lime\n", p.Name))

				}
			}
		}

		fmt.Printf("Players: %d Timeout: %d\n", playercount, timeout)
		io.WriteString(stdin, "list\n")
		io.WriteString(stdin, "time query daytime\n")
		io.WriteString(stdin, "weather query\n")
		if playercount == 0 {
			timeout--
		} else {
			timeout = 20
		}
		time.Sleep(15 * time.Second)
	}

	if err := cmd.Wait(); err != nil {
		running = false
		log.Fatal(err)
	}
	running = false
	stopnow = false
	godmode = false
}

func getPlayerInfo() {
	// Open our jsonFile
	jsonFile, err := os.Open(fmt.Sprintf("%s/%s/playerinfo.json", homedir, gameDir))
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	} else {
		defer jsonFile.Close()

		byteValue, _ := ioutil.ReadAll(jsonFile)
		json.Unmarshal(byteValue, &players)
	}
}

func writeFile() {
	filename := fmt.Sprintf("%s/%s/playerinfo.json", homedir, gameDir)
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
	}

	defer f.Close()

	b, err := json.MarshalIndent(players, "", "  ")
	if err != nil {
		fmt.Println(err)
	}

	nb, err := f.Write(b)
	if err != nil {
		fmt.Println(err)
	}
	f.Sync()
	log.Printf("Wrote %d bytes to %s\n", nb, filename)
}

func copy(src, dst string) (int64, error) {
	src = fmt.Sprintf("%s/%s/%s", homedir, gameDir, src)
	dst = fmt.Sprintf("%s/%s/%s", homedir, gameDir, dst)
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func givestuff(p string) {
	/*
		443 elytra
		743	2E7	netherite_sword	Netherite Sword
		744	2E8	netherite_shovel	Netherite Shovel
		745	2E9	netherite_pickaxe	Netherite Pickaxe
		746	2EA	netherite_axe	Netherite Axe
		747	2EB	netherite_hoe	Netherite Hoe
		748	2EC	netherite_helmet	Netherite Helmet
		749	2ED	netherite_chestplate	Netherite Chestplate
		750	2EE	netherite_leggings	Netherite Leggings
		751	2EF	netherite_boots	Netherite Boots

		Commands:
		xp 256L @a

		// hidden room Base
		-58 85 -32
		// Base
		-54 85 -34

		// Sword
		give @p netherite_sword
		enchant @p looting 3
		enchant @p unbreaking 3
		enchant @p fire_aspect 2
		enchant @p knockback 2
		enchant @p mending 1


	*/
}
