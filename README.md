# MineManager


## Get updated server from Mojang
```
https://www.minecraft.net/en-us/download/server/bedrock/ 
```
## Extract to folder

mkdir bedrock-1.19
cd bedrock-1.19
unzip bedrock-server-1.19.63.01.zip

## to start the server
```
cd work/minemanager
export PATH=$PATH:./ ; go run main.go
```

## install nginx and configure passthru
```
sudo apt install nginx
vim /etc/nginx/sites-available/default
        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                proxy_pass http://127.0.0.1:8443;
                #try_files $uri $uri/ =404;
        }

nginx -t
systemctl enable nginx
service nginx restart
```
