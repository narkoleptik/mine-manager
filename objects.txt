give @p netherite_sword
enchant @p looting 3
enchant @p unbreaking 3
enchant @p fire_aspect 2
enchant @p knockback 2
enchant @p sharpness 5
enchant @p mending 1

give @p netherite_pickaxe
enchant @p fortune 3
enchant @p unbreaking 3
enchant @p efficiency 5
enchant @p mending 1

give @p netherite_pickaxe
enchant @p sliktouch
enchant @p unbreaking 3
enchant @p efficiency 5
enchant @p mending 1

give @p netherite_axe
enchant @p smite 5
enchant @p fortune 3
enchant @p unbreaking 3
enchant @p mending 1

give @p netherite_shovel
enchant @p silk_touch
enchant @p unbreaking 3
enchant @p efficiency 5
enchant @p mending 1

give @p netherite_helmet
enchant @p aqua_affinity 1
enchant @p protection 4
enchant @p thorns 3
enchant @p respiration 3
enchant @p unbreaking 3
enchant @p mending 1

give @p netherite_chestplate
enchant @p protection 4
enchant @p thorns 3
enchant @p unbreaking 3
enchant @p mending 1

give @p netherite_leggings
enchant @p protection 4
enchant @p thorns 3
enchant @p unbreaking 3
enchant @p mending 1

give @p netherite_boots
enchant @p feather_falling 4
enchant @p soul_speed 3
enchant @p depth_strider 3
enchant @p protection 4
enchant @p unbreaking 3
enchant @p mending 1

give @p elytra
enchant @p unbreaking 3
enchant @p mending 1

give @p shield
enchant @p unbreaking 3
enchant @p mending 1

